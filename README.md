# Abelhinha Dashboard
![pipeline](https://gitlab.com/abelhinha/dashboard/badges/master/pipeline.svg "Pipeline")
Link Produção: [Abelhinha Dashboard](https://dashboard.abelhinha.tk/)

Projeto em VueJS / Typescript do Dashboard do Abelhinha.tk

## Setup do projeto
Para instalar o ambiente do projeto executar o seguinte comando:
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
