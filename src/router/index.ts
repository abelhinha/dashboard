import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import PaymentsList from '../views/Authenticated/PaymentsList.vue'
import MyPayments from '../views/Authenticated/MyPayments.vue'
import CardsList from '../views/Authenticated/CardsList.vue'
import PaymentDetails from '../views/Authenticated/PaymentDetails.vue'

import AuthHome from '../views/Authenticated/Home.vue';
import { AuthService } from "../services/Auth";


Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    alias: '/',
    name: 'login',
    component: Login,
    beforeEnter: redirectIfAuth,

  },
  {
    path: '/register',
    name: 'register',
    component: Register,
  },
  {
    path: '/logout',
    name: 'logout',
    component : () => {
      AuthService.getInstance().logout();
      router.push('/');
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('../views/Auth.vue'),
    beforeEnter: isAuthenticated,
    children : [
      {
        path: "/",
        name: "dashboardHome",
        components : {
          "auth-view" : AuthHome,
        }
      },
      {
        path: "/car/:id",
        name: "car",
        components: {
          "auth-view" : () => import('../views/Authenticated/Car.vue')
        }
      },
      {
        path: "/rent",
        name: "rental",
        components: {
          "auth-view" : () => import('@/views/Authenticated/Rent.vue')
        }
      },
      {
        path: 'payments',
        name: 'payments',
        components: {
          "auth-view": () => import('@/views/Authenticated/PaymentList.vue')
        }
      },


      {
        path: 'cardsList',
        name: 'cardsList',
        components: {
          "auth-view": CardsList
        }
      },

      {
        path: 'paymentDetails/:id',
        name: 'paymentDetails',
        components: {
          "auth-view": PaymentDetails
        }
      },
      
    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
];

/**
 *  Authentication Middleware
 * 
 */
function isAuthenticated(to : any, from :any, next: any) {

  if(AuthService.getInstance().isAuthenticated()) {
    next();
  } else {
    router.push('/login');
  }
}

function redirectIfAuth(to : any, from :any, next: any) {
  if(AuthService.getInstance().isAuthenticated()) {
    router.push('/dashboard');
  } else {
    next();
  }
}


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass : "is-active",
  routes
})

export default router
