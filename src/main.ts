import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy';
import VueMeta from 'vue-meta'
import VeeValidate from 'vee-validate'

import { Icon}  from 'leaflet';
import 'leaflet/dist/leaflet.css';


delete (Icon.Default.prototype as any)._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});


Vue.use(VeeValidate, {
    events : ""
})

//import 'buefy/dist/buefy.css';



Vue.use(Buefy, {
  defaultIconPack: 'fas',
});
Vue.use(VueMeta);


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
