import { Subscriber} from "./Subscriber";

export class Observable<T> {


    private subscribers : Subscriber<T | null>[] = [];    

    subscribe(subscriber: Subscriber<T | null> ) : void {
        this.subscribers.push(subscriber);

    }

    emit(data : T | null , err: any)   {
        for (const subscriber of this.subscribers) {
            subscriber(data, err);
        }
    }
}

