import { IIterator } from './IIterator'
import { IIterableCollection } from './IIterableCollection';
import { CollectionIterator } from './CollectionIterator';

export class Collection<T> implements IIterableCollection<T> {

    private collection : Array<T>;

    constructor() {
        this.collection = new Array<T>();
    }

    add(element : T): void {
        this.collection.push(element);
    }

    length (): number {
        return this.collection.length;
    }

    remove(element: T): void {
        const index = this.collection.indexOf(element);
        if (index >= 0) {
            this.collection.splice(index, 1);
        }
    }

    get(index: number): T {
        return this.collection[index];
    }

    createIterator(): IIterator<T> {
        return new CollectionIterator(this);
    }

}
