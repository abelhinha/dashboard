import {IIterator} from './IIterator'

export interface IIterableCollection<T> {

    createIterator(): IIterator<T>

}
