import { Collection } from './Collection';
import { IIterator } from './IIterator';

export class CollectionIterator<T> implements IIterator<T> {

    private collection : Collection<T>
    private position = 0;

    constructor (collection: Collection<T>) {
        this.collection = collection;
    }

    next(): T {
        let value = this.collection.get(this.position);
        this.position++;
        return value;
    }
    
    hasNext(): boolean {
        if (this.position >= this.collection.length() || this.collection.get(this.position) == null) {
            return false;
        }
        return true;
    }

}