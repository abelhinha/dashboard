import {  StorageService } from "../StorageService";
import { AxiosRequestConfig } from "axios";

/**
 * 
 *  @description DEPENDENT OF HttpService
 */
export function authorize(target: any, propertyKey: string, descriptor: PropertyDescriptor) {


    const oldMethod = descriptor.value;

    descriptor.value = function(...args : any[]) {

        const token = StorageService.getInstance().getVal("_token");


        if(token) {
            Object.assign(this, { request : {
                headers : {
                    "Authorization" : `Bearer ${token}` 
                }
            } as AxiosRequestConfig})

        }
        
        return oldMethod.apply(this,args);

    }
  

    return descriptor;
}