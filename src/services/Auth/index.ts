import {ApiService} from '../ApiService';
import {Observable, Subscriber} from '../../patterns/Observer';
import axios , {AxiosInstance, AxiosRequestConfig} from 'axios';
import { StorageService } from '../StorageService'
import { HttpService } from '../HttpService';


export interface AuthModel {
    email : string,
    token : string,
}


/**
 * Authentication Service (API CALLS) Singleton
 * @class
 */
export class AuthService extends HttpService{


    /**
     * @property Stores the AuthService instance (singleton)
     */
    private static instance : AuthService;
    
    private constructor() {
        super();
        this.axios = ApiService.getInstance();
    }

    /**
     *  @returns Instance of AuthService
     */

    public static getInstance() : AuthService {
        if(AuthService.instance == null) {
            AuthService.instance = new AuthService();
        }

        return AuthService.instance;
    }



    /**
     * @description Authenticates a user with the backend, if credentials are correct returns a Observable with a token and email
     * @param {string} email User email
     * @param {string} password User password
     * @returns Observable of AuthModel
     */
    public authenticate(email : string, password: string) : Observable<AuthModel>{
        
        let observable : Observable<AuthModel> = new Observable<AuthModel>();

        this.axios.post("/users/login",{"Email" : email, "Password": password})
        .then((response : any) => {
            observable.emit(response.data, null);
        })
        .catch(err => {
            observable.emit(null, err.response);
        });

        

        return observable;
    }

    public isAuthenticated() : boolean {
        return StorageService.getInstance().getVal("_token") != null;
    }

    /**
     * @todo
     */
    public logout() : void {
       StorageService
        .getInstance()
        .removeVal("_token");
    }


}