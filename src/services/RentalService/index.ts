import {ApiService} from '../ApiService';
import {Observable} from '../../patterns/Observer';
import { RentalModel } from '../../models/Rental';
import { authorize } from "../Auth/auth.decorator";
import { HttpService } from '../HttpService';

export class RentalService extends HttpService {
    private static instance: RentalService;

    private constructor() {
        super();
        this.axios = ApiService.getInstance();
    }

    public static getInstance() : RentalService {
        if(RentalService.instance == null)
            RentalService.instance = new RentalService();

        return RentalService.instance;
    }
    
    @authorize
    public getAll() : Observable<RentalModel[]> {
        const observable : Observable<RentalModel[]> = new Observable<RentalModel[]>();
        this.axios.get("/rentals", this.request)
            .then(response => {
                observable.emit(response.data as RentalModel[], null);
            })
            .catch(err => {
                observable.emit(null, err);
            });

        return observable;

    }

    @authorize
    public getCurrent() : Observable<RentalModel>  {
        let observable: Observable<RentalModel> = new Observable<RentalModel>();

        this.axios.get('/rentals/current',this.request)
        .then(response => {
            observable.emit(response.data, null);
        })
        .catch(err => {
            observable.emit(null, err.response);
        });

        return observable;

    }

    @authorize
    public startRent(carId: number) : Observable<boolean> {
        let observable : Observable<boolean> = new Observable<boolean>();

        this.axios.post('/rentals/start', { VehicleId : carId }, this.request)
        .then(response => {
            observable.emit(true,null);
        })
        .catch(err => {
            observable.emit(false,err);
        });

        return observable;
    }

    @authorize
    public finishRent() : Observable<any> {
        let observable : Observable<any> = new Observable<any>();
        this.axios.put("/rentals/finish", {} , this.request)
        .then(response => {
            observable.emit(response.data,null);
        })
        .catch(err => {
            observable.emit(null,err);
        });

        return observable;
    }

}