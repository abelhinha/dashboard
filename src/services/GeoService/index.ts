import {Observable, Subscriber} from '../../patterns/Observer';
import { Coord } from './ICoord'
/**
 * @description Singleton - Geo Localization Service
 * @class
 */

export class GeoService {
    
    private static instance : GeoService;

    private constructor () {
        if (!navigator.geolocation) { 
            console.error("GEOLOCALIZATION NOT SUPPORTED!")
        }

    }

    public static getInstance() : GeoService {
        if(GeoService.instance == null) {
            GeoService.instance = new GeoService();
        }

        return GeoService.instance;
    }

    public getUserPos() : Observable<Coord> {

        const observable = new Observable<Coord>();

        navigator.geolocation.getCurrentPosition(position => {

            observable.emit({
                latitude : position.coords.latitude,
                longitude : position.coords.longitude,
            }, null)
        }, 
        (err) => {
            observable.emit(null,err);
        });

        return observable;
    }

}