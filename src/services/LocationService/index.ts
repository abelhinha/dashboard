import { HttpService } from '../HttpService';
import {Observable, Subscriber} from '../../patterns/Observer';
import { ApiService } from '../ApiService';

export class LocationService extends HttpService{

    private static instance: LocationService;

    private constructor() {
        super();

        this.axios = ApiService.getInstance();
        this.axios.defaults.baseURL = "https://locationiq.com/v1/";
    }

    public static getInstance() : LocationService {
        if(LocationService.instance == null)
            LocationService.instance = new LocationService();

        return LocationService.instance;
    }

    getAddressFromCoords(lat: number, long : number) : Observable<any> {
        let observable : Observable<any> = new Observable<any>();

        alert("IM CALLED");

        this.axios.get("reverse_sandbox.php", { params : {
            format : "json",
            "accept-language" : "pt",
            lat,
            long
        }})
        .then(response => {
            console.log("RESPONSE", response);
            observable.emit(response.data ,null);
        })
        .catch(err => {
            observable.emit(null,err);
        });

        return observable;

    }


}