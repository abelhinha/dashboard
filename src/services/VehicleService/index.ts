import {ApiService} from '../ApiService';
import {HttpService} from '../HttpService';
import {AxiosInstance, AxiosRequestConfig} from 'axios';
import {Observable, Subscriber} from '../../patterns/Observer';
import { VehicleModel } from '../../models';
import { authorize } from "../Auth/auth.decorator";

export {VehicleModel};

export class VehicleService extends HttpService {
  
    private static instance: VehicleService;
    

    private constructor() {
        super();
        this.axios = ApiService.getInstance();
    }


    public static getInstance() : VehicleService {
        if(VehicleService.instance == null)
            VehicleService.instance = new VehicleService();

        return VehicleService.instance;
    }
    

    @authorize
    public getAll() : Observable<VehicleModel[]> {

        const observable : Observable<VehicleModel[]> = new Observable<VehicleModel[]>();
        this.axios.get("/vehicles", this.request)
            .then(response => {
                observable.emit(response.data as VehicleModel[], null);
            })
            .catch(err => {
                observable.emit(null, err);
            });

        return observable;

    }

    @authorize
    public get(id: number): Observable<VehicleModel> {
        const observable: Observable<VehicleModel> = new Observable<VehicleModel>();
        this.axios.get(`/vehicles/${id}`, this.request)
            .then(response => {
                observable.emit(response.data as VehicleModel, null);
            })
            .catch(err => {
                observable.emit(null, err);
            })
        return observable;
    }

}