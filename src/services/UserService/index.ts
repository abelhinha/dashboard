import {ApiService} from '../ApiService';
import {HttpService} from '../HttpService'
import {Observable, Subscriber} from '../../patterns/Observer';
import axios , {AxiosInstance, AxiosRequestConfig} from 'axios';
import { RegisterModel } from './RegisterModel';


export class UserService extends HttpService {

    private static instance : UserService;



    public constructor() {
        super();
        this.axios = ApiService.getInstance();
    }

    public static getInstance() : UserService {
        
        if(UserService.instance == null) {
            UserService.instance = new UserService();
        }

        return UserService.instance;
    }

    public register(registerData : any) {
        let observable : Observable<any> = new Observable<any>();

        let ajaxConfig : AxiosRequestConfig = {
            headers : {
                "Content-Type" : "multipart/form-data"
            }
        }

        this.axios.post('/users/register', registerData , ajaxConfig)
        .then( response => {
                observable.emit(response.data , null);
        })

        .catch(err => {
                observable.emit(null, err);
        })

        return observable;

    }

}
