import {ApiService} from '../ApiService';
import {Observable, Subscriber} from '../../patterns/Observer';
import { CardModel } from '../../models/Card';
import { authorize } from "../Auth/auth.decorator";
import {AxiosRequestConfig} from 'axios';
import { HttpService } from '../HttpService';

export class CardService extends HttpService {
    private static instance: CardService;

    private constructor() {
        super();
        this.axios = ApiService.getInstance();
    }

    public static getInstance() : CardService {
        if(CardService.instance == null)
            CardService.instance = new CardService();

        return CardService.instance;
    }
    
    @authorize
    public getAllCards() : Observable<CardModel[]> {
        const observable : Observable<CardModel[]> = new Observable<CardModel[]>();
        this.axios.get("/cards", this.request)
            .then(response => {
                observable.emit(response.data as CardModel[], null);
            })
            .catch(err => {
                observable.emit(null, err);
            });

        return observable;

    }



    @authorize
    public addCard(cardData : any) {
        let observable : Observable<any> = new Observable<any>();

        this.axios.post('/cards', cardData , this.request)
        .then( response => {
                observable.emit(response , null);
        })

        .catch(err => {
                observable.emit(null, err);
        })

        return observable;
    }


}