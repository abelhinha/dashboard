import axios, { AxiosInstance } from 'axios';

/**
 *  Wrapper Class for Axios
 *  @class
 * 
 */
export class ApiService {


      private static instance : AxiosInstance;

      private ApiService() {}

      /**
       * Get instance of Axios
       * @method
       */
      public static getInstance() : AxiosInstance {
          if(ApiService.instance == null) {
              this.instance = axios.create({
                baseURL : "https://abelhinha-api.herokuapp.com/",
                headers : {
                   "content-type" :  'application/json'
                }
              });
          }

          return ApiService.instance;
      }


}