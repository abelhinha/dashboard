

export class  StorageService {

    private static instance : StorageService ;

    private constructor() {
        if (typeof(Storage) === "undefined") {
            console.error("BROWSER NOT SUPPORTED");
        }
    }

    public static getInstance() {
        if(StorageService.instance == null) {
            StorageService.instance = new StorageService();
        }

        return StorageService.instance;
    }


    public setVal( index : string, val : string) : void {
        localStorage.setItem(index, val);

    }

    public getVal( index : string) : string | null {
        return localStorage.getItem(index);
    }

    public removeVal(index: string) : void {
        localStorage.removeItem(index);
    }

}