import { HttpService } from '../HttpService';
import { Observable } from '../../patterns/Observer';
import { authorize } from "../Auth/auth.decorator";
import { ApiService } from '../ApiService';
import { PaymentModel } from '@/models/Payment';

export class PaymentService extends HttpService {

    private static instance : PaymentService;

    private constructor() {
        super();
        this.axios = ApiService.getInstance();
    }
    
    public static getInstance() : PaymentService {
        if(PaymentService.instance == null)
            PaymentService.instance = new PaymentService();

        return PaymentService.instance;
    }

    @authorize
    public getAll() : Observable<PaymentModel[]> {
        let observable : Observable<PaymentModel[]> = new Observable<PaymentModel[]>();

        this.axios.get('/payments', this.request)
        .then(response => {
            observable.emit(response.data, null)
        })
        .catch((err) => {
            observable.emit(null,err);
        })

        return observable;
    }

    @authorize
    public get(id : number) : Observable<PaymentModel> {
        let observable : Observable<PaymentModel> = new Observable<PaymentModel>();

        this.axios.get(`/payments/${id}`, this.request)
        .then(response => {
            observable.emit(response.data, null);
        })
        .catch(err => {
            observable.emit(null, err.response);
        });

        return observable;
    }

    @authorize
    public finishPayment(paymentId: number, cardId: number) {
        let observable : Observable<any> = new Observable<any>();
        this.axios.put("/payments", {
            CardID: cardId,
            PaymentID: paymentId
        } , this.request)
        .then(response => {
            observable.emit(response.data, null);
        })
        .catch(err => {
            observable.emit(null, err.response);
        });

        return observable;

    }

}