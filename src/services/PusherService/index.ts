import Pusher from 'pusher-js';


const KEY = "707dc13d1555028b71b3";

export class PusherService {

    private static pusher : Pusher.Pusher;

    private constructor() {
        PusherService.pusher = new Pusher(KEY, {
            cluster : "eu",
            forceTLS : true
        });
    }

    public static getInstance() : Pusher.Pusher {
        if(PusherService.pusher == null) {
            new PusherService();
        }

        return PusherService.pusher;
    }

}