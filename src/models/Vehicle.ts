export interface VehicleModel {
    id : number,
    brand: string,
    horsePower: number,
    fuelType: string,
    license: string,
    seats: number,
    color: string,
    rentPrice: string,
    position: {
        latitude  : number,
        longitude : number,
    }
    speed : number,
    rentedBy : number
    image: string
}

