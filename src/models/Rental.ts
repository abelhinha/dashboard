import { VehicleService, VehicleModel } from "../services/VehicleService";

export interface RentalModel {
    id : number
    startDate: string
    endDate: string
    vehicle: VehicleModel
    currentPrice?: number
}

