import { RentalModel } from './Rental';
import { CardModel } from './Card';

export interface PaymentModel {
    id: number
    rental: RentalModel
    status: string
    price: number
}
