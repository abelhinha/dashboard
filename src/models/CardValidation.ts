export interface CardValidation {
    regex: RegExp,
    cardType: string
    image: string,
    lightcolor: string,
    darkcolor: string
}

